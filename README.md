# Screw-Social-Spying

Outbound iptables rules and IP block lists for local and/or NAT forwarding blocking of major social media companies, including ByteDance (TikTok), Meta (Facebook) and Twitter.

Long-term to include auto-update daemon to maintain current lists in /etc/antisocial/